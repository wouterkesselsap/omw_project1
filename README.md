
## Short description
This script and report is written for the first project of the course Computational Physics at the faculty of Applied Physics, TU Delft.

## Getting started
main.ipynb runs the complete simulation. The folder init contains non-tunable parameters and functions to create an initial state.
The folder comp contains all functions for the evolution calculations.
The folder post contains all functions for errorestimation, validation and  visualisation.

## Authors
* Olaf C. Dreijer (4296621)
* Maxim Q. Capelle (4268628), and
* Wouter Kessels (4201248).