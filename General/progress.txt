1. What have you achieved in the last week?
2. What goal are you trying to reach right now?
3. How are you checking whether you correctly reach the goal?
   illustrate this with data, possibly also refer to the relevant bits of code
4. If you are facing a problem, what is its likely source?
5. What steps do you plan to undertake to solve the problem?


18/2:
1. There were a lot of difficulties in getting git working in combinations with JupyterLab. Wouter has spent hours figuring    out how to deal with the .ipynb_checkpoints/ folder. This gave many problems when synchronizing with other users but by      use of .gitignore we solved this. Furthermore, branches are made so that the three of us can nicely play around. This        still gives merging problems, though. Regarding the content of project 1, Max and Olaf have written out how to simulate      the box containing the argon atoms and how to calculate the shortest interdistances.
2. We are currently trying to fix the merging problems (.swp files are causing issues) between different branches and          writing the code for the particle interdistances.
3. We have created the main.py and parameters.py and are trying to get these files correctly on all branches.
4. .swp files that git is creating and tries to merge.
5. We have asked a TA, who helped us. He too did not know exactly what was happening but some magic along the way by            aborting the merge and then trying again seems to do the trick.
From now on we really want to keep us busy with the physics.


25/2:
1. - Solving all issues with Git (Wouter)
   - Time evolution of particles' positions, velocities and forces (Olaf)
   - Creating mesh grid inside the box (Max)
   - Creating tunable parameter file (Max, Wouter)
   - Herstructure planning, taking into account possible setbacks. Trying not to dive to deep into it immediately (all)
   - Creating a file with separate functions needed for calculations (Olaf)
2. Making all scripts, created by different people, cooperate with each other.
3. From main.py we want load in all parameters and experiment settings and subsequently run the desired python scripts.        Whether this works as aspected will be clear after we have visualized what is happening in the box and whether it matches    our expectations of the particles' motions.
4. We can expect that using different scripts and function, developed by multiple users, can raise problems due to assigning    for example a wrong data type to some function.
5. We try to work on the project together simultaneously as much as possible so that we immediately can discuss what other      people need and what data types are best to use.


4/3:
1. getting animation working, it works! (Wouter)
adding descriptions to all functions for clarity (Olaf)
creating the initial state of all particles according to a mesh of grid points (Max)
calculating energy as a function of time (Olaf)
matching box dimensions, number of particles (correct density of argon gas) and time steps for valid physical results (Olaf, Wouter)


11/3:
1.
- Almost all for-loops are replaced in the calculations of time evolution of the particles' positions, velocities, forces     and energies. (Olaf + Max)
- Functions are written regarding temperature transition (e.g. scalefactor.py and assemble function) (Olaf + Max)
- Trying to accurately calculate the pressure in the particle box (observables.py) (Wouter)

2. 
a. Resolving why sometimes the total energy makes an enormous jump of many orders of magnitude.
b. Strange behaviour in Kinetic Energy. Within the first few timesteps it jumps to high values and then drops again.
c. Resolving wrong pressure calculations.

3.
a. The energy should be approximately constant. Small fluctuations in energy can be the result of the finite step              approximation, but the fluctuations should only stay small.
b. Also the fluctuations in kinetic energy should stay small.
c. The pressure should stay approximately constant in the box. Now, the pressure fluctuates a lot and also gives negative      values, which of course is physically impossible.

4. 
a. The finite step approximation can be the source of fluctuations but we do not know the source of the large jumps in        energy yet. It might be that somehow two electrons penetrate each other's hard core repulsion area and get accelerated      quickly due to the twelfth power.
b. It might come from a too high initial potential.
c. The negative values are probably due to the inner product of the position vectors with the force vectors.

5.
a. Really take a close look on when exactly the energy jump occurs and what the exact positions, mutual distances,            velocities and forces are.
b. Trying to figure out what the best way is to initialize the system.
c. Calculate the pressure according to the second formula in exercise 8.3.c, which sums over all collisions. This              calculation will probably only be valid for large amounts of particles in order to have enough collisions at each time      step.


18/3:
- Writing script to estimate errors (Max)
- Writing script observables (Heat capacity and pair correlation) (Olaf)
- Finding and solving error in time evolution (Olaf and Wouter)
-

26/3:
Writing report (Olaf, Wouter, Max)
Finalising script (Olaf, Wouter)
Making full script PEP8 conform (Max)