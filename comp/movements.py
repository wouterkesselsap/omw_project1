# import modules
import numpy as np

def func_force(X, L, D, N):
    """
    Function to determine the total forces on all particles.
    last modified: 20-03-2019
    
    Parameters
    ----------
    X : [N,D] matrix
        positions of all particles 
    L : float
        size of the box
    D : float
        number of spatial dimensions
    N : float
        number of particles
    
    Returns
    -------
    F : [N,D] matrix
        total forces on all particles 
    U : float
        Total potential energy of the system 
    dr_mag-np.eye(N) : [N,N] matrix
        all mutual distances between all particles
    """
    
    x = X*np.ones([N,N,D])                                              # Positions of all particles ([N,N,D] array)
    dx = np.transpose(x, (1,0,2)) - x                                   # Distances between all particle pairs ([N,N,D] array)
    dx = (dx + L/2)%L - L/2                                             # Boundary conditions
    dr_mag = np.linalg.norm(dx, axis = 2)                               # Magnitude of distances between particle pairs
    dr_mag += np.eye(N)                                                 # Set diagonal elements to 1 to avoid dividing by zero
    
    U_matrix = 4*(dr_mag**(-12) - dr_mag**(-6))                         # Matrix of potential energies
    U_array = np.sum(U_matrix, axis=0)                                  # Total potential energy of every particle
    U = np.sum(U_array)/2                                               # Total potential energy. Deviding by 2 to compensate for double counting
    
    F_mag = -24*((dr_mag**6 -2 + np.eye(N))/(dr_mag**14))               # Not really the magnitude ;)
    F = np.transpose(dx, (2,0,1)) * F_mag                               # All forces in [D,N,N] array
    F = np.sum(F, axis = 2)                                             # All forces on all particles ([D,N] matrix)
    F = F.transpose()                                                   # Transpose matrix to obtain desired [N,D] matrix

    # return
    return F, U, dr_mag-np.eye(N)
    

    
def func_position(x0, v0, f, L, h):
    """
    Function to calculate the new positions of N particles based on the old positions and velocities.
    last modified: 22-03-2019
    
    Parameters
    ----------
    x0 : [N,D] matrix
        positions of all particles at time 0
    v0 : [N,D] matrix
        velocities of all particles at time 0
    f : [N,D] matrix
        forces on all particles at time 0 
    L : float
        size of the box 
    h : float
        timestep 
    diff : [N,D] matrix
        differences between particle positions at times 0 and 1 
    
    Returns
    -------
    x1 : [N,D] matrix
        positions of all particles at time 1
    """
    
    x1 = x0 + h*v0 + ((h**2)/2)*f                   # New positions based on current positions, velocities and forces
    diff = x1 - x0                                  # Distance traveled. Used for diffusion
    x1 = np.remainder(x1,L)                         # Account for the periodic boundary conditions
    
    # return
    return x1, diff



def func_velocity(v0, f0, f1, h):
    """
    Function to calculate the new velocities of N particles based on the old velocities and forces and the new forces.
    last modified: 05-03-2019
    
    Parameters
    ----------
    v0 : [N,D] matrix
        velocities of all particles at time 0
    f0 : [N,D] matrix
        forces on all particles at time 0 
    f1 : [N,D] matrix
        forces on all particles at time 1 
    h : float
        timestep 
    
    Returns
    -------
    v1 : [N,D] matrix
        velocities of all particles at time 1
    E_kin : float
        total kinetic energy of all particles at time 1
    """
    v1 = v0 + (h/2)*(f1 + f0)                               # New velocities based on current velocities and forces and new forces ([N,D] matrix)
    E_kin_array = .5*(np.linalg.norm(v1, axis=1))**2        # New kinetic energy for every particle
    E_kin = np.sum(E_kin_array)

    # return
    return v1, E_kin



def steps(x0, v0, f0, L, h):
    """
    Function to calculate the positions and velocities of all particles at time 1 based on the positions and velocities of all particles at time 0.
    last modified: 22-03-2019
    
    Parameters
    ----------
    x0 : [N,D] matrix
        positions of all particles at time 0
    v0 : [N,D] matrix
        velocities of all particles at time 0
    f : [N,D] matrix
        forces on all particles at time 0 
    L : float
        size of the box 
    h : float
        timestep
    
    Returns
    -------
    x1 : [N,D] matrix
        positions of all particles at time 1 
    v1 : [N,D] matrix
        velocities of all particles at time 1 
    E : float
        total kinetic energy of all particles at time 1
    U1 : float
        total potential energy at time 1
    f1 : [N,D] matrix
        total forces on all particles at time 1 
    dr1 : [N,N] matrix
        all mutial distances between particles 
    diff : [N,D] matrix
        differences between particle positions at times 0 and 1 
    """
    
    D = x0.shape[1]                                 # Number of spatial dimensions
    N = x0.shape[0]                                 # Number of particles
    
    x1, diff = func_position(x0, v0, f0, L, h)      
    f1, U1, dr1 = func_force(x1, L, D, N)
    v1, E = func_velocity(v0, f0, f1, h)
    
    # return
    return x1, v1, E, U1, f1, dr1, diff

def scalefactor(N, kbT, E_kin_nat, E_kin_selection):
    """
    function to generate a scalefactor when imposing a different temperature. Works for both physical and natural units
    last modified: 2019-03-05
    
    Parameters
    ----------
    N : float
        number of particles
    kbT : float
        boltzmann constant times imposed temperature, [-] or [J = m^2 kg s^-2] 
    E_kin : float
        with computed kinetic energy, [-] or [J] 
         
    Returns
    -------
    scalefactor : float
        lambda: velocity scalefactor
    """
    
    # average energy
    E_kin_average = np.average(E_kin_nat[-E_kin_selection:])
    
    # calculate scale factor    
    scalefactor = (((N-1)*3*kbT)/(2*E_kin_average))**(1/2)
    
    # return
    return(scalefactor)
    
def assemble(x0, v0,
             N_steps, N_argon, L,
             h, h_settemp,
             temp, temp_final, temp_delta,
             E_kin_selection):
    """
    Assembles two (N_steps, N_argon, topology) arrays with the time evolution of the particles' positions and velocities.
    Here N_steps corresponds to the number of time steps to be considered.
    Both input and output in natural units.
    
    Parameters
    ----------
    x0 : [N,D] matrix
        positions of all N particles at the initial time 
    v0 : [N,D] matrix
        velocities of all N particles at the initial time
    N_steps : float
        number of timesteps
    L : float
        size of the box 
    h : float
        timestep
    h_settemp : float
        number of timesteps after which the temperature is scaled
    temp_current : float
        current temperature
    temp_final : float
        goal temperature
    temp_delta : float
        stepsize temperature at h_settemp
        
    Returns
    -------
    X : (N_steps, N_argon, topology) array
        with the time evolution of the particle positions
    V : (N_steps, N_argon, topology) array
        with the time evolution of the particle velocities
    E_kin : ([1,N_steps - 1] array)
        total kinetic energy of the system for every step 
    U : ([1,N_steps - 1] array)
        total potential energy of the system for every step 
    D : (N_steps, N_argon, topology) array
        with the time evolution of the mutual distances between particles
    diffusion : ([1,N_steps] array)
        averaged diffusion of all particles 
    """
    
    # allocate
    X = np.zeros((N_steps,x0.shape[0],v0.shape[1]))
    V = np.zeros((N_steps,x0.shape[0],v0.shape[1]))
    D = np.zeros((N_steps,x0.shape[0],x0.shape[0]))
    
    # Forces on / potential energy of all particles at the initial time
    f0, U0, d0 = func_force(x0, L, x0.shape[1], x0.shape[0])            
    X[0] = x0
    V[0] = v0
    D[0] = d0
    E_kin = ([])
    U = ([])
    n = 0
    diff = 0
    diffusion = 0
    
    for t in range(1,N_steps):
        # Calculate new positions, velocities, energies
        X[t], V[t], E_kin_1, U1, f1, D[t], diff1 = steps(X[t-1], V[t-1], f0, L, h)
        diff += diff1
        diffusion = np.append(diffusion, np.mean(np.linalg.norm(diff**2, axis=1)))
        
        # Rescale
        if t % h_settemp == 0:
            if abs(temp - temp_final) > temp_delta:
                # set temperature
                if temp > temp_final:
                    temp = temp - temp_delta 
                elif temp < temp_final:
                    temp = temp + temp_delta
                    
                # calculate scale factor
                scale_lambda = scalefactor(N_argon, temp, E_kin, E_kin_selection)
            
                # rescale velocities
                V[t] = V[t]*scale_lambda
                
            elif temp == temp_final:
                # Keep Temperature constant
                if n<10:
                    diff = 0
                    n +=1                               
                    # calculate scale factor
                    scale_lambda = scalefactor(N_argon, temp, E_kin, E_kin_selection)
            
                    # rescale velocities
                    V[t] = V[t]*scale_lambda
        
        # Save new positions, velocities, energies
        E_kin = np.append(E_kin, E_kin_1)
        U = np.append(U, U1)
        f0 = f1
        
    # return
    return X[n*h_settemp:], V[n*h_settemp:], E_kin[n*h_settemp:], U[n*h_settemp:], D[n*h_settemp:], diffusion[n*h_settemp:]


