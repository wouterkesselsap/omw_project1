# import modules
import numpy as np
import math


def pressure_coll(D, V, L, kbT, h):
    """
    Calculates the pressure inside the particle box at each time step, based on collisions. Formula from Exercize 8.3.c from the book "Molecular Dynamics" (see background reading at GitLab).
    
    Parameters
    ----------
    D : ((N_time_steps, N_particles, N_particles) np.array, natural units)
        Matrix with the mutual distances between particles at each time step 
    V : ((N_steps, N_argon, topology) array, natural units)
        Matrix with the time evolution of the particle velocities 
    L : float
        Length of the particle box (natural units)
    kbT : float
        Energy, kb*T/epsilon (natural units)
    
    Returns
    -------
    P :  ((N_time_steps) np.array)
        Array with the pressure inside the particle box at each time step
    """
    
    r_coll = 1                           # radius sigma from particle position defines edge hard core repulsion
                                         # if particles are closer to each other than this, it is counted as a collision
    coll = (D < r_coll)                  # matrix with booleans of collision happening between particles per time step
    D_coll = coll*D                      # matrix with all mutual distances of particles that are in collision per time step
    
    V_length = np.linalg.norm(V, axis=2) # matrix with length of velocity of each particle at each time step
    v_av = np.mean(V_length**2, axis=1)  # average squared velocity per time step (np.array(N_time_steps))
    
    V_diff = np.zeros((V_length.shape[0],V_length.shape[1],V_length.shape[1]))
    for t in range(V_length.shape[0]):
        v_temp = np.transpose(V_length[t]*np.ones((V_length.shape[1],V_length.shape[1])))
        V_diff[t] = v_temp - np.transpose(v_temp)
    V_diff = abs(np.triu(V_diff))        # set lower triangle to zero to use every pair only once
    
    P = np.zeros(D_coll.shape[0])
    for t in range(D_coll.shape[0]):
        P[t] = 1 + (V.shape[1]*v_av[t]*h)**(-1)*np.sum(D_coll[t]*V_diff[t])
    
    # return
    return P


def pressure_scalar(D, L, kbT):
    """
    Calculates time-averaged pressure inside the particle box.
    
    Parameters
    ----------
    D : ((N_time_steps, N_particles, N_particles) np.array, natural units)
        Matrix with the mutual distances between particles at each time step 
    L : float
        Length of the particle box (natural units)
    kbT : float
        Energy, kb*T/epsilon (natural units)
    
    Returns
    -------
    P  :  Time-averaged pressure (natural units)
    """
    D += np.eye(D.shape[1])                                         # Set diagonal elements to 1    
    dUdr = D**(-7)-2*D**(-13)                                       
    dUdr += np.eye(D.shape[1])                                      # Set diagonal elements to 0
    S = np.sum(24*D*dUdr)                                           # Note: also a summation over time
    P = 1 - (3*D.shape[1]*kbT)**(-1)*0.5*S/D.shape[0]
    
    # return
    return P


def specific_heat(E_kin, N):
    """
    function to calculate the specific heat
    last modified: 22-03-2019
    
    Parameters
    ----------
    E_kin : array
        array of total kinetic energy for every timestep
    N : float
        number of argon particles
         
    Returns
    -------
    Cv : array
        specific heat
    n : float
        number of timesteps over which the energies are averaged
    """
    # set number of timesteps over which the energies are averaged
    if E_kin.shape[0]>6:
        n = 5
    else:
        n = 1
    
    # allocate
    T = E_kin.shape[0]
    dE_mean = ([])
    E_mean = ([])
    
    # calculate average energy
    for t in range(n, T):
        E_mean = np.append(E_mean, np.mean(E_kin[t-n:t])**2)
        dE_mean = np.append(dE_mean, np.mean(E_kin[t-n:t]**2) - np.mean(E_kin[t-n:t])**2)

    # calculate specific heat
    Cv = (2/(3*N) - dE_mean/E_mean)**(-1)
    
    # return
    return Cv, n


def pair_correlation(L, N, R):
    """
    function to calculate the pair correlation function
    last modified: 19-03-2019
    
    Parameters
    ----------
    L : float
        Size of the box
    N : float
        Number of argon particles
    R : [N,N,timesteps]
        matrix of all distances between particles at all times
         
    Returns
    -------
    G : array
        Pair correlation function
    r : array
        Distances for which the pair correlation function is defined
    dA : array
        All distances between all peaks in the pair correlation function
        
    Raises
    ------
    print("Not enough particles to determine a pair correlation function")
        When not enough particles to determine a pair correlation function (obviously)
    """
    
    if N>4:
        M = N                                                              # Number of points in linspace
    
        R = np.mean(R, axis=0)                                             # Mean of all mutual distances
        dr = L/M
        r = np.linspace(0,L,L/dr)
    
        n = np.histogram(R,r)[0]                                           # Make histogram of mutual particle distances
        n[0] = 0                                                           # Set value at r=0 to zero to avoid "self-correlation"
    
        r = np.delete(r,0)                                                 # r[0] = 0
        r -= (r[2]-r[1])/2                                                 # Places points r on the middle of each interval dr
        G = 2*(L**3)/(N*(N-1))*(n/(4*math.pi*r**2*dr))                     # Pair correlation function
    
    
        peaks = np.where((G[1:-1] > G[0:-2]) * (G[1:-1] > G[2:]))[0] + 1   # Finds locations of all peaks
        G_peak = G[peaks]                                                  
        r_peak = r[peaks]
    
        A = r_peak*np.ones([r_peak.shape[0], r_peak.shape[0]])
        dA = A - A.transpose()                                             # Difference between all positions of all peaks
        dA = np.tril(dA.transpose(), -1).transpose()                       # Only keep positive values
        dA = dA.reshape(dA.shape[0]*dA.shape[1],)                          # Change shape into an array
        dA = dA[np.where(dA != 0)[0]]
        dA = np.sort(dA)
    else:
        dA = 0
        G = 0
        r = 0
        print("Not enough particles to determine a pair correlation function")
    
    # return
    return G,r, dA


def specific_heat_total(E_kin, N):
    """
    function to calculate the specific heat averaged over total time
    last modified: 22-03-2019
    
    Parameters
    ----------
    E_kin : array
        Kinetic energy of the system at all times
    N : float
        Number of argon particles
         
    Returns
    -------
    Cv : float
        Heat capacity, calculated using averages over total simulated time
    """
    
    E_mean = np.mean(E_kin)**2
    dE_mean = np.mean(E_kin**2) - E_mean
    Cv = (2/(3*N) - dE_mean/E_mean)**(-1)
    
    # return
    return Cv