"""
Module to plot graphs of the physical observables.
All input is in natural units.
"""

import matplotlib.pyplot as plt
import numpy as np

# Natural units
sigma    = 3.405E-10                # Natural scalefactor for distance (meters)
tau      = 2.15E-12                 # Natural scalefactor for time (seconds)
kb       = 1.381E-23                # Boltzmann constant (Joule/Kelvin)
T_ref    = 119.8                    # Reference temperature (Kelvin)
mass     = 6.6335209*10**-26        # Mass of the argon atom (kg)
epsilon  = T_ref*kb                 # Natural units for energy (Joule)


def energy(K, U, std_E, std_K, std_U, error_alpha, h, N, units='physical'):
    """
    Plots kinetic, potential and total energy as function of time.
    
    Parameters
    ----------
    K :  ([1,N_steps - 1] array)
        Total kinetic energy of the system for every step
    U :  ([1,N_steps - 1] array)
        Total potential energy of the system for every step
    std_E : float
        Standard deviation of the total energy
    std_K : float
        Standard deviation of the kinetic energy
    std_U : float
        Standard deviation of the potential energy
    error_alpha : float
        Transparancy of errorbar shade
    h : float
        Length of time step
    N : float
        Number of particles
    units : string
        Natural or physical units as output
    
    Raises
    ------
    plot
    IOError
        When units field unequel to either 'natural' or 'physical'
    """
    
    if units=='natural':
        pass
    elif units=='physical':          # Convert to physical units
        h = h*tau             
        K = K*epsilon
        U = U*epsilon
        std_E = std_E*epsilon
        std_K = std_K*epsilon
        std_U = std_U*epsilon
    else:
        raise IOError("units field must be either 'natural' or 'physical'")
    
    plt.figure(figsize=[8,4])
    plt.plot(h*np.array(range(K.shape[0])), (K + U), color='orange')
    plt.fill_between(h*np.array(range(K.shape[0])),
                     (K + U - std_E), (K + U + std_E),
                     alpha=error_alpha, color='orange')

    plt.plot(h*np.array(range(K.shape[0])), K, '--', color='blue')
    plt.fill_between(h*np.array(range(K.shape[0])),
                     (K - std_K), (K + std_K),
                     alpha=error_alpha, color='blue')

    plt.plot(h*np.array(range(U.shape[0])), U, ':', color='green')
    plt.fill_between(h*np.array(range(K.shape[0])),
                     (U - std_U), (U + std_U),
                     alpha=error_alpha, color='green')
    
    if units=='natural':
        plt.xlabel("Time [$\\tau$]")
        plt.ylabel("Energy [$\\epsilon$]")
    elif units=='physical':
        plt.xlabel("Time [s]")
        plt.ylabel("Energy [Joule]")
    plt.legend(["Total", "Kinetic", "Potential"])
    plt.title("Energy plots with {} particles".format(N))
    plt.show()



def pressure(P, std_P, error_alpha, h, density, K, N, units='physical'):
    """
    Plots the pressure inside the particle box as a function of time.
    
    Parameters
    ----------
    P :  ((N_time_steps) np.array)
        Array with the pressure inside the particle box at each time step
    std_P : float
        Standard deviation of the pressure
    error_alpha : float
        Transparancy of errorbar shade
    h : float
        Length of time step
    density : float
        Particle density inside the box
    K : ([1,N_steps - 1] array)
        Total kinetic energy of the system for every step 
    N : float
        Number of particles
    units : string
        Natural or physical units as output
    
    Raises
    ------
    plot
    IOError
        When units field unequel to either 'natural' or 'physical'
    """
    
    if units=='natural':
        pass
    elif units=='physical':          # Convert to physical units
        density = density/(sigma**3)
        P_conv = density*kb*np.mean(T_ref*2*K/((N-1)*3))
        P = P*P_conv
        std_P = std_P*P_conv
        h = h*tau             
        K = K*epsilon
    else:
        raise IOError("units field must be either 'natural' or 'physical'")
    
    plt.figure(figsize=[8,4])
    plt.plot(h*np.array(range(1,P.shape[0])), P[1:], color='blue')
    plt.fill_between(h*np.array(range(1,P.shape[0])),
                     P[1:] + std_P, P[1:] - std_P,
                     alpha=error_alpha, color='blue')
    
    if units=='natural':
        plt.xlabel("Time [$\\tau$]")
        plt.ylabel("Pressure [$\\epsilon \\sigma^{-3}$]")
    elif units=='physical':
        plt.xlabel("Time [s]")
        plt.ylabel("Pressure [Pa]")
    plt.title("Pressure with {} particles".format(N))
    plt.legend(["Data","Standard deviation"])
    plt.show()



def specific_heat(CV, std_CV, error_alpha, N_CV, h, N, units='physical'):
    """
    Plots the specific heat (heat capacity) as a function of time.
    
    Parameters
    ----------
    CV : array
        Specific heat
    std_CV : float
        Standard deviation of the specific heat
    error_alpha : float
        Transparancy of errorbar shade
    N_CV : float
        Number of timesteps to average over
    h : float
        Length of time step
    N : float
        Number of particles
    units : string
        Natural or physical units as output
    
    Raises
    ------
    plot
    IOError
        When units field unequel to either 'natural' or 'physical'
    """
    
    CV = CV/N
    if units=='natural':
        pass
    elif units=='physical':          # Convert to physical units
        CV = CV*kb
        std_CV = std_CV*kb
        h = h*tau
    else:
        raise IOError("units field must be either 'natural' or 'physical'")
    
    plt.figure(figsize=[8,4])
    plt.plot(h*np.array(range(N_CV + 1, CV.shape[0] + N_CV + 1)), CV, color='blue')
    plt.fill_between(h*np.array(range(N_CV + 1, CV.shape[0] + N_CV + 1)),
                     (CV + std_CV), (CV - std_CV),
                     alpha=error_alpha, color='blue')
    if units=='natural':
        plt.xlabel("Time [$\\tau$]")
        plt.ylabel("Specific heat [$k_B$]")
    elif units=='physical':
        plt.xlabel("Time [s]")
        plt.ylabel("Specific heat [J/K]")
    plt.title("Specific heat per particle averaged over {} timesteps".format(N_CV))
    plt.legend(["Data","Standard deviation"])
    plt.show()



def pair_correlation(r, G, N, units='physical'):
    """
    Plots the pair correlation as a function of distance.
    
    Parameters
    ----------
    G : array
        Pair correlation function
    r : array
        Distances for which the pair correlation function is defined
    N : float
        Number of particles
    units : string
        Natural or physical units as output
    
    Raises
    ------
    plot
    IOError
        When units field unequel to either 'natural' or 'physical'
    """
    if units=='natural':
        pass
    elif units=='physical':          # Convert to physical units
        r = r*sigma
    else:
        raise IOError("units field must be either 'natural' or 'physical'")
    
    plt.figure(figsize=[8,4])
    plt.plot(r, G)
    if units=='natural':
        plt.xlabel("Distances between particles [$\\sigma$]")
    elif units=='physical':
        plt.xlabel("Distances between particles [m]")
    plt.ylabel("Pair correlation")
    plt.title("Pair correlation with {} particles".format(N))
    plt.show()



def diffusion(diff, N, N_steps, h, units='physical'):
    """
    Plots the diffusion as a function of time.
    
    Parameters
    ----------
    diff : ((N_time_steps) np.array)      Vector with the average distance traveled by the particles with respect to t=0 
    N : float
        Number of particles inside the box
    N_steps : float
        Number of timesteps
    h : float
        Length of timestep
    units : string
        Natural or physical units as output
    
    Raises
    ------
    plot
    IOError
        When units field unequel to either 'natural' or 'physical'
    """
    
    if units=='natural':
        pass
    elif units=='physical':          # Convert to physical units
        diff = diff*sigma
        h = h*tau
    else:
        raise IOError("units field must be either 'natural' or 'physical'")
    
    plt.figure(figsize=[8,4])
    plt.plot(h*np.array(range(diff.shape[0])), diff)
    if units=='natural':
        plt.xlabel("Time [$\\tau$]")
        plt.ylabel("Diffusion [$\\sigma$]")
    elif units=='physical':
        plt.xlabel("Time [s]")
        plt.ylabel("Diffusion [m]")
    plt.title("Average diffusion of {} particles".format(N))
    plt.show()


    
def temperature(T, h, N, units='physical'):
    """
    Plots the temperature as a function of time.
    
    Parameters
    T : array
        Temperature per time step
    h : float
        Length of timestep
    N  : float
        Number of particles inside the box
    units : string
        Natural or physical units as output
    
    Raises
    ------
    plot
    IOError
        When units field unequel to either 'natural' or 'physical'
    """
    
    if units=='natural':
        pass
    elif units=='physical':          # Convert to physical units
        T = T*T_ref
        h = h*tau
    else:
        raise IOError("units field must be either 'natural' or 'physical'")
    
    plt.figure(figsize=[8,4])
    plt.plot(h*np.array(range(T.shape[0])), T)
    if units=='natural':
        plt.xlabel("Time [$\\tau$]")
        plt.ylabel("Temperature [$\\epsilon k_B^{-1}$]")
    elif units=='physical':
        plt.xlabel("Time [s]")
        plt.ylabel("Temperature [K]")
    plt.title("Temperature of {} particles".format(N))
    plt.show()