# import modules
import numpy as np

def create_table(density, T, E_kin, P, N, U, Cv):
    """
    Function for a clear comparison with literature data and data as found in by the simulated model.
    
    Parameters
    ---------
    density : float
    T : float
        temperature
    E_kin : array
        Kinetic energy
    P : float
        Pressure
    N : float
        Number of particles
    U : array
        Potential energy
    Cv : float
        Specific heat
    
    Returns
    -------
    Table with simulated data and data found in literature.
    """
    
    print("In natural units:")

    # write table
    table = [
        ["", "Density (set)", "Temperature (set)", "Temperature", "Pressure (beta*P/rho)", "Potential energy", "Specific heat"],
        ["Values book (1)", 0.88, 1.0, 0.990, 2.98, -5.704, "1.5 (Ideal gas)"],
        ["Values book (2)", 0.80, 1.0, 1.010, 1.31, -5.271, "1.5 (Ideal gas)"],
        ["Values book (3)", 0.70, 1.0, 1.014, 1.06, -4.662, "1.5 (Ideal gas)"],
        ["Our values", f"{density:.2e}", f"{T:.2e}", f"{np.mean(2*E_kin[-800:]/((N-1)*3)):.2e}", f"{P:.2e}", f"{np.mean(U[-800:])/N:.2e}", f"{Cv/N:.2e}"],
    ]
    
    # return
    return(table)
       
def print_table(table):
    """Function to print table"""
    longest_cols = [
        (max([len(str(row[i])) for row in table]) + 3)
        for i in range(len(table[0]))
    ]
    row_format = "".join(["{:>" + str(longest_col) + "}" for longest_col in longest_cols])
    for row in table:
        print(row_format.format(*row))
        

def validation_table(density, T, E_kin, P, N, U, Cv):
    """Function to set up table for comparison with literature values"""
    table = create_table(density, T, E_kin, P, N, U, Cv)
    print_table(table)