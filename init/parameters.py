"""Non-tunable parameters for conversion between physical and natural units"""
sigma    = 3.405E-10                # Natural scalefactor for distance (meters)
tau      = 2.15E-12                 # Natural scalefactor for time (seconds)
kb       = 1.381E-23                # Boltzmann constant (Joule/Kelvin)
T_ref    = 119.8                    # Reference temperature (Kelvin)
mass     = 6.6335209*10**-26        # Mass of the argon atom (kg)
epsilon  = T_ref*kb                 # Natural units for energy (Joule)

"""Semi-tunable model parameterss"""
topology = 3                        # Number of dimensions
error_alpha   = 0.2                 # Transparancy of errorbar shade
