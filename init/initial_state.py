# import modules
import numpy as np
import math
import random

def build(
        N, L, T_init,
        topology, org='fcc'):
    """
    Sets initial particle positions and velocities.
    
    Parameters
    ----------
    N : float
        Number of particles inside the particle box
    L : float
        Size of the particle box
    T_init : float
        kbT/epsilon, initial temperature
    topology : float
        Number of dimensions
    org : string
        Way to spatially organize the particles, either 'fcc' or 'cubic'
    
    Returns
    -------
    x0 : (N, topology) np.array
        Particle positions 
    v0 : (N, topology) np.array
        Particle velocities 
    density : float
        Particle density after initialization, as fcc(N,L) can change the number of particles
        
    Raise
    -----
    IOError
        org has not been given a correct string input
    """
    
    # initial positions
    if org == "fcc":
        # Initialize particles in an fcc lattice and calculate new density
        N, x0 = fcc(N, L)
        density = N/(L**3)         
    elif org == "cubic":
        # Initialize particles in a cubic lattice, randomly distributed
        x0 = cubic(N, topology, L, T_init)
        density = N/(L**3)
    else:
        raise IOError("org has not been given a correct string input, should be either 'fcc' or 'cubic'")
    
    # initial velocities
    v0 = velocity(T_init, N, topology)
    
    # return
    return x0, v0, density, N


def cubic(
        N_particles,
        topology, L_box,
        temperature):
    """
    Initializes particles in a cubic lattice, randomly distributed.
    
    A box of size (L_box,L_box,L_box (in 3D)) is divided in N_grid**3 gridpoints.
    All this gridpoints have an internal distance d = L_box/N_grid, measured in orthogonal directions.
    These gridpoints have positions n_x = 0, 1, 2, ... . Similar for n_y and n_z. 
    Using random.sample() every particle is assigned a unique gridpoint.
    Subsequently these gridpoints are translated to their corresponding x, y and z coordinates.
    
    Parameters
    ----------
    N_particles : float
        number of Argonatoms
    topology: float
        Number of dimensions
    L_box: float
        length of box; size to be determined
    N_grid: float
        number of gridpoints in one direction
    temperature: float
        Initial temperature
        
    Returns
    -------
    x: (N_particles, topology) array
        x, y, and z positions for every particle
        
    Raises
    ------
    ValueError
        When the number of particles is larger than the number of gridpoints.
    ValueError
        When the topology is not equal to 1, 2 or 3.
    """
    # Position
    N_grid = math.ceil(N_particles**(1/topology))
    d = L_box/N_grid
    
    # Create random sample
    if N_particles > (N_grid**topology):
        raise ValueError(f"N_particles ({N_particles}) is larger than number of gridpoints ({N_grid}**{topology}={N_grid**topology})")
    else:
        n_positions = np.array(random.sample((range(0,N_grid**topology)),N_particles)) 

    # Select position number
    if topology == 3:
        n_x = (n_positions-(n_positions % N_grid**2))/N_grid**2
        n_y = n_positions % N_grid                              
        n_z = np.floor(n_positions / N_grid)  % N_grid          
        n = np.vstack([n_x, n_y, n_z])                          
    elif topology == 2:
        n_x = (n_positions-(n_positions % N_grid))/N_grid      
        n_y = n_positions % N_grid                              
        n = np.vstack([n_x, n_y])                               
    elif topology == 1:        
        n_x = n_positions                                     
        n = np.vstack([n_x])                                    
    else:
        raise ValueError("Topology is not equal to 1, 2, or 3")

    # Generate position coordinates
    x = np.transpose(n*d)
    
    # return
    return x


def fcc(N,L):
    """
    Function to create initial position of N particles in a box of (L,L,L), ordered in a FCC-lattice.
    last modified: 2019/03/26
    
    Parameters
    ----------
    N : float
        number of particles
    L : float
        size of the box
        
    Returns
    -------
    N: float
        number of particles that is in accordance with a FCC-latice
    pos_initial : (N,[x,y,z])-array
        (x,y,z) positions of particles
    
    """
    
    # Check whether number of particles N is in accordance with a FCC-latice
    if N not in [4, 32, 108, 256, 500, 864]:
        print(f"Number of particles is set set from {N} to {round((N/4)**(1/3))**3*4} in fcc(N,L)")
        N = round((N/4)**(1/3))**3*4
    
    # cell properties
    n = round((N/4)**(1/3))  # number of cells in 1D
    d   = L/(2*n)            # size of a cell

    # cell vertices: positions of particle 1
    vertice_basis = np.linspace(0,L, n+1)[0:-1]     
    pos_x = np.repeat(vertice_basis,n**2)           
    pos_y = np.tile(np.repeat(vertice_basis, n), n) 
    pos_z = np.tile(vertice_basis, n**2)
    pos_basis = np.transpose(np.array([pos_x, pos_y, pos_z])) + d/2

    # add positions of particle 2, 3 and 4
    pos_1 = pos_basis + [d,d,0]
    pos_2 = pos_basis + [d,0,d]
    pos_3 = pos_basis + [0,d,d]

    # positions for N particles: (N,[x,y,z])-array
    pos_initial = np.concatenate((pos_basis, pos_1, pos_2, pos_3),axis=0)
    
    # returns
    return N, pos_initial


def velocity(T, N, topology):
    """
    Function to compute initial velocity for N particles in (x,y,z) direction.
    last modified: 2019-03-15
    
    Parameters
    ----------
    T : float
        temperature
    N : float
        number of particles
    topology : float
        number of dimensions
        
    Returns
    -------
    v : (N,[x,y,z])-array
        initial velocity 
    """
    
    # Magnitude
    v_magnitude = np.random.normal(0, math.sqrt(2*T), N)

    # Direction
    v_direction = np.random.rand(N,topology)
    # # normalize direction
    v_length = np.linalg.norm(v_direction,axis=1)
    v_normalized = v_direction / v_length[:,None]

    # Velocity
    v = np.multiply(v_normalized, v_magnitude[:, np.newaxis])
    
    # return
    return v